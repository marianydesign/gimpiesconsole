﻿using System;

namespace GimpiesConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // username en password variable
            String username;
            String password;
            // schoenen
            String schoennaam;
            String schoenmaat;
            String schoenprijs;

            // array with different users with different roles
            String[,] accnts = { { "admin", "admin", "admin" }, { "inkoop", "inkoop", "inkoop" }, { "verkoop", "verkoop", "verkoop" } };

            // array met schoenen
            String[,] schoenen = { { "nike", "39", "200" }, { "puma", "43", "120" }, { "adidas", "45", "210" } };

            int row;
            bool uitgelogged = true;
            bool isValideUser = false;
            bool menu = false;

            for (int x = 3; x >= 1; x--)
            {
                if (uitgelogged == true)
                {
                    Console.WriteLine("-=+=------------------------------=+=-");

                    Console.WriteLine("U heeft nog " + x + " pogingen.");

                    Console.WriteLine("Wat is uw gebruikersnaam?");

                    Console.WriteLine("-=+=------------------------------=+=-");

                    username = Console.ReadLine();

                    Console.WriteLine("-=+=------------------------------=+=-");

                    Console.WriteLine("Wat is uw wachtwoord?");

                    Console.WriteLine("-=+=------------------------------=+=-");

                    password = Console.ReadLine();


                    for (row = 0; row < accnts.GetLength(0); row++)
                    {
                        if (username.Equals(accnts[row, 0]) && password.Equals(accnts[row, 1]))
                        {
                            Console.WriteLine("-=+=------------------------------=+=-");

                            Console.WriteLine("Welkom " + accnts[row, 0] + "!");

                            Console.WriteLine("-=+=------------------------------=+=-");

                            isValideUser = true;
                            uitgelogged = false;
                        }



                        // Wanneer de user is gevalideerd, kan hij daarna checken welke user is ingelogd, zodat daarna er iets mee gedaan kan worden, zoals een bepaald menu weergeven.
                        if (isValideUser)
                            // Hier gaat de code terug naar de array helemaal bovenaan en check in kolom "2" welke rol de ingelogde/gevalideerde gebruiker heeft. Dan kiest hij daarna wat de gebruiker te zien krijgt.

                            if (accnts[row, 2] == "admin")
                            {
                                int number;
                                if (menu == true)
                                    Console.WriteLine("-=+=------------------------------=+=-");

                                Console.WriteLine("Welkom admin!");

                                Console.WriteLine("Druk op 1 om de users te zien.");

                                Console.WriteLine("Druk op 2 om het schoenenoverzicht te zien.");

                                Console.WriteLine("Druk op 3 om uit te loggen.");

                                Console.WriteLine("Druk op 4 voor Exit.");

                                Console.WriteLine("-=+=------------------------------=+=-");

                                number = Convert.ToInt32(Console.ReadLine());


                                // Intoetsen cijfer 1 dan voert hij deze code uit
                                if (number == 1)
                                {
                                    Console.WriteLine("-=+=------------------------------=+=-");

                                    Console.WriteLine("Naam     Wachtwoord      Functie");

                                    // Declareert int users om alle gebruikers langs te gaan die in de array bovenaan de code staan "opgeslagen". Met Console.WriteLine geeft hij de gebruikers weer.
                                    int users = 0;

                                    for (int i = 0; i < accnts.GetLength(0); i++)

                                    {

                                        Console.WriteLine(accnts[users, 0] + "      " + accnts[users, 1] + "        " + accnts[users, 2]);

                                        users++;

                                    }
                                    Console.WriteLine("-=+=------------------------------=+=-");
                                }

                                // Bij intoetsen cijfer 2 voert hij onderstaande code uit.
                                if (number == 2)
                                {

                                    Console.Clear();
                                    Console.WriteLine("-=+=------------------------------=+=-");

                                    Console.WriteLine("SchoenNaam     Schoenmaat      Schoenprijs");

                                    // Declareert int schoen om alle schoenen langs te gaan die in de array bovenaan de code staan "opgeslagen". Met Console.WriteLine geeft hij de schoenen weer.
                                    int schoen = 0;

                                    for (int i = 0; i < schoenen.GetLength(0); i++)
                                    {

                                        Console.WriteLine(schoenen[schoen, 0] + "      " + schoenen[schoen, 1] + "        " + schoenen[schoen, 2]);

                                        schoen++;
                                    }
                                    Console.WriteLine("-=+=------------------------------=+=-");
                                    //Console.Read(); om tijdelijk een console te laten staan, gaat weg na enter
                                    Console.Read();
                                    //Hier het menu laden

                                    //Menu in een functie zetten, bijvoorbeeld ShowMenu()
                                    //Hier uitlezen: ShowMenu();
                                }

                                // Bij intoetsen cijfer 3 voert hij onderstaande code uit.
                                if (number == 3)
                                {
                                    Console.Clear();

                                    uitgelogged = true;

                                    isValideUser = false;

                                    x = 4;
                                }

                                if (number == 0)
                                {

                                    Console.WriteLine("Pipo");
                                }

                            }
                    }
                }
            }
        }
    }
}

